/**
 *
 */
package org.evolvis.portal.portletutils.portletregistry.liferay;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSecurityException;
import javax.portlet.PortletURL;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

/**
 * This class synthesizes portlet urls in liferay style.
 *
 * @author pcorne
 */
public class PortletURLImpl implements PortletURL {

    /** The base url. */
    private String baseUrl = null;

    /** The portlet id. */
    private String portletId = null;

    /** The parameter map. */
    private final Map<String, List<String>> parameterMap = new HashMap<String, List<String>>();

    /** The lifecycle. */
    private String lifecycle = PortletRequest.RENDER_PHASE;

    /** The url type. */
    private String urlType = "0";

    /** The state. */
    private final String state = "normal";

    /** The portlet mode. */
    private PortletMode portletMode = PortletMode.VIEW;

    /**
     * Sets the lifecycle.
     *
     * Possible values are gatherhed via PortletRequest.ACTION_PHASE,
     * PortletRequest.RENDER_PHASE or PortletRequest.RESOURCE_PHASE
     *
     * @param newLifecycle
     *            the lifecycle to set
     */
    public void setLifecycle(final String newLifecycle) {
        if (PortletRequest.ACTION_PHASE.equals(newLifecycle)
                || PortletRequest.RENDER_PHASE.equals(newLifecycle)
                || PortletRequest.RESOURCE_PHASE.equals(newLifecycle)) {
            this.lifecycle = newLifecycle;
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the string representation of the URL.
     *
     * @return the string
     */
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(baseUrl);
        sb.append('?');
        appendParam(sb, "p_p_id", portletId);
        appendParam(sb, "p_p_lifecycle", lifecycleToURLValue(lifecycle));
        appendParam(sb, "p_p_url_type", urlType);
        appendParam(sb, "p_p_state", state);
        appendParam(sb, "p_p_mode", portletMode.toString());
        for (final String name : parameterMap.keySet()) {
            if (parameterMap.get(name).size() == 1) {
                appendParam(sb, "_" + portletId + "_" + name, parameterMap.get(
                        name).get(0));
            } else {
                int idx = 0;
                for (final String value : parameterMap.get(name)) {
                    appendParam(sb, "_" + portletId + "_" + name + "[" + idx
                            + "]", value);
                    idx++;
                }
            }
        }
        return sb.toString();
    }

    /**
     * Lifecycle to url value.
     *
     * @param lifecycle2
     *            the lifecycle2
     *
     * @return the string
     */
    private String lifecycleToURLValue(final String lifecycle2) {
        // This is ordered by the probability of the values to speed things up
        if (PortletRequest.ACTION_PHASE.equals(lifecycle2)) {
            return "1";
        }
        if (PortletRequest.RENDER_PHASE.equals(lifecycle2)) {
            return "0";
        }
        if (PortletRequest.RESOURCE_PHASE.equals(lifecycle2)) {
            return "2";
        }
        throw new IllegalArgumentException();
    }

    /**
     * Append param.
     *
     * @param sb
     *            the sb
     * @param paramName
     *            the param name
     * @param param
     *            the param
     */
    private void appendParam(final StringBuilder sb, final String paramName,
            final String param) {
        if (sb.charAt(sb.length() - 1) != '?') {
            sb.append('&');
        }
        sb.append(paramName);
        sb.append("=");
        sb.append(param);
    }

    /**
     * Instantiates a new portlet url liferay impl.
     *
     * @param newBaseUrl
     *            the new base url
     * @param newPortletId
     *            the new portlet id
     */
    public PortletURLImpl(final String newBaseUrl,
            final String newPortletId) {
        baseUrl = newBaseUrl;
        this.portletId = newPortletId;
    }

    /** {@inheritDoc} */
    public PortletMode getPortletMode() {
        return portletMode;
    }

    /** {@inheritDoc} */
    public WindowState getWindowState() {
        throw new NotImplementedException();
        // return null;
    }

    /** {@inheritDoc} */
    public void removePublicRenderParameter(final String name) {
        throw new NotImplementedException();
    }

    /** {@inheritDoc} */
    public void setPortletMode(final PortletMode mode)
            throws PortletModeException {
        portletMode = mode;
    }

    /** {@inheritDoc} */
    public void setWindowState(final WindowState windowState)
            throws WindowStateException {
        throw new NotImplementedException();
    }

    /** {@inheritDoc} */
    public void addProperty(final String key, final String value) {
        throw new NotImplementedException();
    }

    /** {@inheritDoc} */
    public Map<String, String[]> getParameterMap() {
        final Map<String, String[]> result = new HashMap<String, String[]>();
        for (final String name : parameterMap.keySet()) {
            final String[] data = new String[parameterMap.get(name).size()];
            parameterMap.get(name).toArray(data);
            result.put(name, data);
        }
        return result;
    }

    /** {@inheritDoc} */
    public void setParameter(final String name, final String value) {
    	parameterMap.put(name, new ArrayList<String>(1));
    	parameterMap.get(name).add(value);
    }

    /** {@inheritDoc} */
    public void setParameter(final String name, final String[] values) {
        parameterMap.put(name, new ArrayList<String>());
        for (final String item : values) {
            parameterMap.get(name).add(item);
        }
    }

    /** {@inheritDoc} */
    public void setParameters(final Map<String, String[]> parameters) {
        parameterMap.clear();
        for (final String name : parameters.keySet()) {
            parameterMap.put(name, new ArrayList<String>());
            for (final String item : parameters.get(name)) {
                parameterMap.get(name).add(item);
            }
        }
    }

    /**
     * @param urlType
     *            the urlType to set
     */
    public void setUrlType(final String urlType) {
        this.urlType = urlType;
    }

    /** {@inheritDoc} */
    public void setProperty(final String key, final String value) {
        throw new NotImplementedException();
    }

    /** {@inheritDoc} */
    public void setSecure(final boolean secure) throws PortletSecurityException {
        throw new NotImplementedException();
    }

    /** {@inheritDoc} */
    public void write(final Writer writer) throws IOException {
        write(writer, false);
    }

    /** {@inheritDoc} */
    public void write(final Writer writer, final boolean escapeXml)
            throws IOException {
        String toString = toString();

        if (escapeXml) {
            toString = escapeURL(toString);
        }

        writer.write(toString);
    }

    private String escapeURL(String urlString) {
    	return urlString.replaceAll("&", "&amp;")
        	.replaceAll("<", "&lt;")
        	.replaceAll(">", "&gt;")
        	.replaceAll("\'", "&#039;")
        	.replaceAll("\"", "&#034;");
    }

    public class NotImplementedException extends RuntimeException {

        /**
		 *
		 */
        private static final long serialVersionUID = 1460936826761367938L;

    }
}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.liferay;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import org.evolvis.portal.portletutils.portletregistry.PortletURLProvider;

/**
 * @author pcorne
 * 
 */
public class PortletURLProviderImpl implements PortletURLProvider {

    /** {@inheritDoc} */
    public PortletURL createActionUrl(final String baseUrl,
            final String portletId) {
        final PortletURLImpl portletURLLiferayImpl = new PortletURLImpl(
                baseUrl, portletId);
        portletURLLiferayImpl.setLifecycle(PortletRequest.ACTION_PHASE);
        return portletURLLiferayImpl;
    }

    /** {@inheritDoc} */
    public PortletURL createRenderUrl(final String baseUrl,
            final String portletId) {
        final PortletURLImpl portletURLLiferayImpl = new PortletURLImpl(
                baseUrl, portletId);
        portletURLLiferayImpl.setLifecycle(PortletRequest.RENDER_PHASE);
        return portletURLLiferayImpl;
    }

    /** {@inheritDoc} */
    public PortletURL createResourceUrl(final String baseUrl,
            final String portletId) {
        final PortletURLImpl portletURLLiferayImpl = new PortletURLImpl(
                baseUrl, portletId);
        portletURLLiferayImpl.setLifecycle(PortletRequest.RESOURCE_PHASE);
        return portletURLLiferayImpl;
    }

}

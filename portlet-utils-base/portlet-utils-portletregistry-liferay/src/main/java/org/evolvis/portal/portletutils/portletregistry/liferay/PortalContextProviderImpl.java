/**
 *
 */
package org.evolvis.portal.portletutils.portletregistry.liferay;

import org.evolvis.portal.portletutils.portletregistry.PortalContextProvider;

import com.liferay.portal.model.Company;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.service.CompanyLocalServiceUtil;

/**
 * Liferay implementation of {@link PortalContextProvider}.
 *
 * This implementation retrieves the identifier of the Portal instance as
 * default context. The actual Portal instance is retrieved from a
 * ThreadLocal-holder which is filled by the Portal according to the request
 * actually processed in this thread. The Liferay web id is used as Portal
 * instance identifier.
 *
 * @author Jens Neumaier, tarent GmbH
 *
 */
public class PortalContextProviderImpl implements PortalContextProvider {

	public String retrievePortalContext() {
		long companyId = CompanyThreadLocal.getCompanyId();
		Company company;
		try {
			company = CompanyLocalServiceUtil.getCompany(companyId);
		} catch (Exception e) {
			return null;
		}
		return company.getWebId();
	}


}

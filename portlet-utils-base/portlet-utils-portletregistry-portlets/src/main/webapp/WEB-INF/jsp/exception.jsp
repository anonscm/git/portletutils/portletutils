<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<h3>General Error</h3>

<p>${exception.localizedMessage == null ? exception : exception.localizedMessage}<br/></p>

<p>${exception.class}</p>

<portlet:renderURL var="homeUrl"><portlet:param name="portletMode" value="view"/></portlet:renderURL>
<p><a href="${homeUrl}">Default View</a></p>

</jsp:root>
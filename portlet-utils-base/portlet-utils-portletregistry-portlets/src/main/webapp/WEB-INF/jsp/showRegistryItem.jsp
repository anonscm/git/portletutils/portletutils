<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<html>
  <head>
  	<meta http-equiv="Refresh" content="0; url=${itemRenderUrl}" />
  	<title>Redirect to ${itemRenderUrl}</title> 
  </head>
  <body>
  	<script type="text/javascript">window.location='${itemRenderUrl}';</script>
  	<noscript>You should be redirected to: <a href="${itemRenderUrl}">${itemRenderUrl}</a></noscript>
  </body>
</html>

</jsp:root>
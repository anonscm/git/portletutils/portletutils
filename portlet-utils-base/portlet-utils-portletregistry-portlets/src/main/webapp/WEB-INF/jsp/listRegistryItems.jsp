<?xml version="1.0" encoding="UTF-8" ?>
<jsp:root version="2.0"
	xmlns:jsp="http://java.sun.com/JSP/Page"
	xmlns:c="http://java.sun.com/jsp/jstl/core"
	xmlns:fmt="http://java.sun.com/jsp/jstl/fmt"
	xmlns:fn="http://java.sun.com/jsp/jstl/functions"
	xmlns:portlet="http://java.sun.com/portlet_2_0"
	xmlns:s="http://www.springframework.org/tags"
	xmlns:form="http://www.springframework.org/tags/form"
>
<jsp:directive.page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false" isThreadSafe="true"/>

<h3>List of RegistryItems</h3>

<ul>
  <c:forEach items="${registryItems}" var="item">
	<li>
		${item.id}, ${item.baseUrl}, ${item.serviceName}, ${item.context}, ${item.portletId}
		<br/>
		<portlet:resourceURL id="showRegistryItem" var="showRegistryItemURL">
			<portlet:param name="itemId" value="${item.id}" />
		</portlet:resourceURL>
		<portlet:actionURL name="deleteRegistryItem" var="deleteRegistryItemURL">
			<portlet:param name="itemId" value="${item.id}" />
		</portlet:actionURL>
		<a href="${showRegistryItemURL}" target="_blank">SHOW</a> | <a href="${deleteRegistryItem}">DELETE</a>
	</li>
  </c:forEach>
</ul>

</jsp:root>
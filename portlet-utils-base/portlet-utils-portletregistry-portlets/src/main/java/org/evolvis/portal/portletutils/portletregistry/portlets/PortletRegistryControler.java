package org.evolvis.portal.portletutils.portletregistry.portlets;

import javax.portlet.PortletURL;

import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;
import org.evolvis.portal.portletutils.portletregistry.exception.PortletRegistryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

@Controller("PortletRegistryControler")
@RequestMapping(value = "view")
public class PortletRegistryControler {

	@Autowired
	@Qualifier("portletRegistry")
	private PortletRegistry portletRegistry;
	
	@RenderMapping
	public String viewRegistryItems(final Model model) {
		model.addAttribute("registryItems", portletRegistry.getAllRegistryItems());
		return "listRegistryItems";
	}
	
	@ResourceMapping("showRegistryItem")
	public String showItem(@RequestParam("itemId") final Long itemId, final Model model) throws PortletRegistryException {
		final PortletRegistryItem registryItem = portletRegistry.getRegistryItemById(itemId);
		final PortletURL itemRenderUrl = portletRegistry.createRenderUrl(registryItem);
		model.addAttribute("itemRenderUrl", itemRenderUrl.toString());
		return "showRegistryItem";
	}
	
	@ActionMapping("deleteRegistryItem")
	public void deleteRegistryItem(@RequestParam("itemId") final Long itemId) {
		portletRegistry.unregisterItem(itemId);
	}
}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.sun;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSecurityException;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.evolvis.portal.portletutils.portletregistry.sun.PortletURLImpl;
import org.evolvis.portal.portletutils.portletregistry.sun.PortletURLImpl.NotImplementedException;
import org.junit.Test;

/**
 * The Class URLCreationTest.
 * 
 * @author pcorne
 */
public class URLCreationTest {
    private static final String fullTargetUrl = "http://localhost:8080/web/guest/shop-checkout?p_p_id=CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk&amp;p_p_lifecycle=1&amp;p_p_url_type=0&amp;p_p_state=normal&amp;p_p_mode=view&amp;p_p_col_id=column-2&amp;_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_action=enterBillingAddress";
    private static final String BASEURL = "http://localhost:8080/web/guest/shop-checkout";
    private static final String PORTLETID = "CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk";
    private static final String URLTYPE = "0";
    private static final String STATE = "normal";
    private static final String MODE = "view";

    @Test
    public void testBaseUrl() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, "");
        assertTrue("BaseURL", liferayImpl.toString().startsWith(BASEURL + '?'));
    }

    @Test
    public void testPortletId() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        checkForParam(liferayImpl, "p_p_id", PORTLETID);
    }

    @Test
    public void testSetLifecycle() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        liferayImpl.setLifecycle(PortletRequest.ACTION_PHASE);
        checkForParam(liferayImpl, "p_p_lifecycle", "1");
        liferayImpl.setLifecycle(PortletRequest.RENDER_PHASE);
        checkForParam(liferayImpl, "p_p_lifecycle", "1");
        liferayImpl.setLifecycle(PortletRequest.RESOURCE_PHASE);
        checkForParam(liferayImpl, "p_p_lifecycle", "2");
    }

    @Test
    public void testSetLifecycleFail() {
        try {
            new PortletURLImpl(BASEURL, PORTLETID).setLifecycle("1");
            fail();
        } catch (final IllegalArgumentException e) {
            e.getClass();
        }
    }

    @Test
    public void testURLType() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        checkForParam(liferayImpl, "p_p_url_type", URLTYPE);
    }

    @Test
    public void testState() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        checkForParam(liferayImpl, "p_p_state", STATE);

    }

    @Test
    public void testMode() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        checkForParam(liferayImpl, "p_p_mode", MODE);
    }

    @Test
    public void testGivenParam() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        liferayImpl.setParameter("action", "enterBillingAddress");
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_action",
                "enterBillingAddress");
    }

    @Test
    public void testGivenParamArray() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        final String[] ids = { "1", "2", "3" };
        liferayImpl.setParameter("ids", ids);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[0]",
                ids[0]);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[1]",
                ids[1]);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[2]",
                ids[2]);
    }

    @Test
    public void testGivenParamsMap() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        final Map<String, String[]> parameters = new HashMap<String, String[]>();
        final String[] ids = { "1", "2", "3" };
        parameters.put("ids", ids);
        liferayImpl.setParameters(parameters);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[0]",
                ids[0]);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[1]",
                ids[1]);
        checkForParam(liferayImpl,
                "_CheckoutPortlet_WAR_eshopportlets_INSTANCE_Q0Zk_ids[2]",
                ids[2]);
    }

    @Test
    public void testPortletMode() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        try {
            liferayImpl.setPortletMode(PortletMode.EDIT);
            assertEquals(PortletMode.EDIT, liferayImpl.getPortletMode());
            liferayImpl.setPortletMode(PortletMode.HELP);
            assertEquals(PortletMode.HELP, liferayImpl.getPortletMode());
            liferayImpl.setPortletMode(PortletMode.VIEW);
            assertEquals(PortletMode.VIEW, liferayImpl.getPortletMode());
        } catch (final PortletModeException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetParameterMap() {
        final PortletURLImpl liferayImpl = new PortletURLImpl(
                BASEURL, PORTLETID);
        assertEquals(0, liferayImpl.getParameterMap().size());
        liferayImpl.setParameter("bla", "blub");
        assertEquals(1, liferayImpl.getParameterMap().size());
        assertTrue(liferayImpl.getParameterMap().containsKey("bla"));
        assertEquals(1, liferayImpl.getParameterMap().get("bla").length);
        assertEquals("blub", liferayImpl.getParameterMap().get("bla")[0]);
        liferayImpl.setParameter("bla", "blub");
    }

    @Test
    public void testAddProperty() {
        try {
            new PortletURLImpl(BASEURL, PORTLETID).addProperty("", "");
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    @Test
    public void testsetProperty() {
        try {
            new PortletURLImpl(BASEURL, PORTLETID).setProperty("", "");
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    @Test
    public void testsetSecure() throws PortletSecurityException {
        try {
            new PortletURLImpl(BASEURL, PORTLETID).setSecure(false);
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    @Test
    public void testsetWindowState() throws WindowStateException {
        try {
            new PortletURLImpl(BASEURL, PORTLETID)
                    .setWindowState(WindowState.MAXIMIZED);
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    @Test
    public void testGetWindowState() {
        try {
            new PortletURLImpl(BASEURL, PORTLETID).getWindowState();
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    @Test
    public void testremovePublicRenderParameter() {
        try {
            new PortletURLImpl(BASEURL, PORTLETID)
                    .removePublicRenderParameter("");
            fail();
        } catch (final NotImplementedException e) {
            e.getClass();
        }
    }

    /**
     * Check for paramName with param value in the URL.
     * 
     * @param liferayImpl
     *            the liferay impl
     * @param paramName
     *            the param name
     * @param param
     *            the param
     */
    private void checkForParam(final PortletURLImpl liferayImpl,
            final String paramName, final String param) {
        final String searchString = ".*[&?]" + Pattern.quote(paramName) + "="
                + Pattern.quote(param) + "(&.*|$)";
        // test the test
        // System.out.println(searchString);
        // System.out.println("abc?" + paramName + "=" + param + "&def");
        // System.out.println(liferayImpl.toString());
        assertTrue("regexptest ? ", ("abc?" + paramName + "=" + param + "&def")
                .matches(searchString));
        assertTrue("regexptest &", ("abc&" + paramName + "=" + param + "&def")
                .matches(searchString));
        assertTrue("regexptest $", ("abc&" + paramName + "=" + param)
                .matches(searchString));

        assertTrue(paramName, liferayImpl.toString().matches(searchString));
    }
}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.sun;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.io.StringWriter;

import org.evolvis.portal.portletutils.portletregistry.sun.PortletURLImpl;
import org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl;
import org.junit.Test;

/**
 * @author pcorne
 * 
 */
public class PortletURLProviderSunImplTest {

    /**
     * Test method for
     * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl#createActionUrl(java.lang.String)}
     * .
     * 
     * @throws IOException
     */
    @Test
    public void testCreateActionUrl() throws IOException {
        final PortletURLProviderImpl portletURLProviderLiferayImpl = new PortletURLProviderImpl();
        final PortletURLImpl portletURLLiferayImpl = (PortletURLImpl) portletURLProviderLiferayImpl
                .createActionUrl("http://test.me:9080/now", "iamaportletid");
        final StringWriter writer = new StringWriter();
        portletURLLiferayImpl.write(writer);
        assertEquals(
                "http://test.me:9080/now?p_p_id=iamaportletid&p_p_lifecycle=1&p_p_url_type=1&p_p_state=normal&p_p_mode=view",
                writer.toString());
    }

    /**
     * Test method for
     * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl#createActionUrl(java.lang.String)}
     * .
     * 
     * @throws IOException
     */
    @Test
    public void testCreateActionUrlEncodedXml() throws IOException {
        final PortletURLProviderImpl portletURLProviderLiferayImpl = new PortletURLProviderImpl();
        final PortletURLImpl portletURLLiferayImpl = (PortletURLImpl) portletURLProviderLiferayImpl
                .createActionUrl("http://test.me:9080/now", "iamaportletid");
        final StringWriter writer = new StringWriter();
        portletURLLiferayImpl.write(writer, true);
        assertEquals(
                "http://test.me:9080/now?p_p_id=iamaportletid&amp;p_p_lifecycle=1&amp;p_p_url_type=1&amp;p_p_state=normal&amp;p_p_mode=view",
                writer.toString());
    }

    /**
     * Test method for
     * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl#createRenderUrl(java.lang.String)}
     * .
     * 
     * @throws IOException
     */
    @Test
    public void testCreateRenderUrl() throws IOException {
        final PortletURLProviderImpl portletURLProviderLiferayImpl = new PortletURLProviderImpl();
        final PortletURLImpl portletURLLiferayImpl = (PortletURLImpl) portletURLProviderLiferayImpl
                .createRenderUrl("http://test.me:9080/now", "iamaportletid");
        final StringWriter writer = new StringWriter();
        portletURLLiferayImpl.write(writer);
        assertEquals(
                "http://test.me:9080/now?p_p_id=iamaportletid&p_p_lifecycle=1&p_p_url_type=0&p_p_state=normal&p_p_mode=view",
                writer.toString());
    }

    /**
     * Test method for
     * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl#createResourceUrl(java.lang.String)}
     * .
     * 
     * @throws IOException
     */
    @Test
    public void testCreateResourceUrl() throws IOException {
        final PortletURLProviderImpl portletURLProviderLiferayImpl = new PortletURLProviderImpl();
        final PortletURLImpl portletURLLiferayImpl = (PortletURLImpl) portletURLProviderLiferayImpl
                .createResourceUrl("http://test.me:9080/now", "iamaportletid");
        final StringWriter writer = new StringWriter();
        portletURLLiferayImpl.write(writer);
        assertEquals(
                "http://test.me:9080/now?p_p_id=iamaportletid&p_p_lifecycle=2&p_p_url_type=0&p_p_state=normal&p_p_mode=view",
                writer.toString());
    }

}

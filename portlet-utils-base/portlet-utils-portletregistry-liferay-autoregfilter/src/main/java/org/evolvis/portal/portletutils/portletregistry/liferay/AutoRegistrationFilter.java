package org.evolvis.portal.portletutils.portletregistry.liferay;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.portlet.PortletException;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.RenderFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;

/**
 * This is a Portlet filter to enable automatic registration of a Portlet to the
 * portletregistry. The filter is written to connect to the ejb implementation.
 * 
 * @author Jens Neumaier, tarent GmbH
 * @author Timo Pick, tarent GmbH
 * 
 */
public class AutoRegistrationFilter implements RenderFilter {

	public static final String FILTER_PARAM_KEY_CONTEXT_PROVIDER_CLASS = "contextProviderClass";

	public static final String FILTER_PARAM_KEY_DATA_PROVIDER_CLASS = "dataProviderClass";

	public static final String FILTER_PARAM_KEY_SERVICE_NAME = "serviceName";

	private static Log log = LogFactory.getLog(AutoRegistrationFilter.class.getName());

	/** The portlet registry JNDI name. */
	protected static final String portletRegistryJNDIName = "portlet-utils-portletregistry-ear/PortletRegistryEJB3Impl/local";

	protected PortletRegistry portletRegistry = null;

	protected Set<String> registeredPortlets = new HashSet<String>();

	protected PortletContextProvider portletContextProvider;

	protected PortletDataProvider portletDataProvider;

	protected String customServiceName;

	/**
	 * Load and initialize the autoregistration ilter. Load implementation
	 * specific context and data providers. Fallback to SUN impl when none are
	 * provided.
	 */
	@SuppressWarnings("unchecked")
	public void init(FilterConfig filterConfig) throws PortletException {

		String contextProviderClassName = filterConfig.getInitParameter(FILTER_PARAM_KEY_CONTEXT_PROVIDER_CLASS);

		try {
			Class<PortletContextProvider> contextProviderClass = null;
			if (contextProviderClassName != null) {
				// load custom data provider
				contextProviderClass = (Class<PortletContextProvider>) Class.forName(contextProviderClassName);
			} else {
				// fallback on default data provider
				contextProviderClass = (Class<PortletContextProvider>) Class.forName(PortletContextProviderLiferayImpl.class.getName());
			}
			log.info("Using context provider class: '" + contextProviderClass.getName() + "'.");
			portletContextProvider = contextProviderClass.newInstance();
		} catch (Exception e) {
			log.error("The context provider could not be resolved to a class. AutoRegistrationFilter is skipped.", e);
			throw new PortletException(e);
		}

		assert portletContextProvider != null;

		String dataProviderClassName = filterConfig.getInitParameter(FILTER_PARAM_KEY_DATA_PROVIDER_CLASS);
		try {
			Class<PortletDataProvider> dataProviderClass = null;
			if (dataProviderClassName != null) {
				// load custom data provider
				dataProviderClass = (Class<PortletDataProvider>) Class.forName(dataProviderClassName);
			} else {
				// fallback on default data provider
				dataProviderClass = (Class<PortletDataProvider>) Class.forName(PortletDataProviderSunImpl.class.getName());
			}
			log.info("Using data provider class: '" + dataProviderClass.getName() + "'.");
			portletDataProvider = dataProviderClass.newInstance();
		} catch (Exception e) {
			log.error("The data provider could not be resolved to a class. AutoRegistrationFilter is skipped.", e);
			throw new PortletException(e);
		}

		assert portletDataProvider != null;

		String configServiceName = filterConfig.getInitParameter(FILTER_PARAM_KEY_SERVICE_NAME);
		if (configServiceName != null) {
			customServiceName = configServiceName;
		}

		try {
			Context context = new InitialContext();
			/*
			 * get registry item and save a reference in this filter
			 */
			portletRegistry = (PortletRegistry) context.lookup(portletRegistryJNDIName);
		} catch (NamingException e) {
			log.error("Error looking up PortletRegistry Service. ", e);
			throw new PortletException(e);
		}

	}

	public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException {
		String context = portletContextProvider.retrievePortletContext(request, response);
		if (context == null) {
			log.error("The PortletContextProvider returned a null context. Skipping.");
			chain.doFilter(request, response);
			return;
		}
		String portletId = portletDataProvider.retrievePortletId(request, response);
		if (portletId == null) {
			log.error("The PortletDataProvider returned a null portletId. Skipping.");
			chain.doFilter(request, response);
			return;
		}
		
		String serviceName = customServiceName != null ? customServiceName : portletDataProvider.retrieveServiceName(request, response);
		if (serviceName == null) {
			log.error("The PortletDataProvider returned null serviceName. Skipping.");
			chain.doFilter(request, response);
			return;
		}

		/*
		 * determine the Portlet's baseURL
		 */
		String baseUrl = retrieveBaseURL(response);

		String uniquePortletIdentifier = new StringBuilder().append(portletId).append(";baseUrl=").append(baseUrl)
				.append(";customContext=").append(context).toString();

		if (!registeredPortlets.contains(uniquePortletIdentifier)) {
			/*
			 * add portletRegistryItem, if not exists
			 */
			if (portletRegistry.getRegistryItemForService(serviceName, context) == null) {
				/*
				 * prepare PortletRegistryItem
				 */
				PortletRegistryItem portletRegistryItem = portletRegistry.createRegistryItem(baseUrl, portletId, serviceName, context);

				/*
				 * register Portlet, log trying to register, success is logged
				 * Portlet Registry
				 */
				log.debug("Trying to register portlet [serviceName='" + serviceName + "', baseUrl='" + baseUrl + "', portletId='"
						+ portletId + "', context='" + context + "'].");
				portletRegistry.registerPortletInstance(portletRegistryItem);
				/*
				 * save Portlet registration in local static set to allow
				 * registration check with reasonable resource requirements on
				 * each portlet request.
				 */
				registeredPortlets.add(uniquePortletIdentifier);
			}
		}

		chain.doFilter(request, response);
	}

	public void destroy() {
		// do nothing
	}

	// helper
	private String extractBaseURL(String fullURL) {
		return fullURL.split("(\\?|;)")[0];
	}

	private String retrieveBaseURL(RenderResponse response) {
		PortletURL portletURL = response.createRenderURL();
		return extractBaseURL(portletURL.toString());
	}
}

package org.evolvis.portal.portletutils.portletregistry.liferay;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.portal.model.Company;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.service.persistence.CompanyUtil;

/**
 * Default implementation for getting the context of a portlet 
 * registration using the Liferay companyId.
 */
public class PortletContextProviderLiferayImpl implements PortletContextProvider {
	
	private static final Log logger = LogFactory.getLog(PortletContextProviderLiferayImpl.class);
	
	/**
	 * Retrieving context field for registering into portlet-registry. The idm
	 * client / active tenant is used as registry context to retrieve the proper
	 * login/registration portlet for a user.
	 */
	public String retrievePortletContext(RenderRequest renderRequest, RenderResponse renderResponse) {
		try {
			// fallback to liferay web id configuration
			Company company = CompanyUtil.findByPrimaryKey(CompanyThreadLocal.getCompanyId());
			String liferayWebIdTenantName = company.getWebId();
			
			return liferayWebIdTenantName;
		} catch (Exception e) {
			logger.error("Error retrieving active tenant for service name generation.", e);
			return null;
		}
	}
}

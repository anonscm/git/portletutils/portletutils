package org.evolvis.portal.portletutils.portletregistry.liferay;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Override this class in your portlet if you wish to control how the context
 * of the RegistryItems is generated in AutoRegistrationFilter.
 */
public interface PortletContextProvider {

	/**
	 * Retrieves context (e.g. path, companyId, tenant) for which a portlet is to be resolved
	 */
	public String retrievePortletContext(RenderRequest renderRequest, RenderResponse renderResponse);
	
}

package org.evolvis.portal.portletutils.portletregistry.liferay;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Portlet data provider for the SUN portlet container residing within a liferay portal
 */
public class PortletDataProviderSunImpl implements PortletDataProvider {
	
	public String retrievePortletId(RenderRequest renderRequest, RenderResponse renderResponse) {
		return (String) renderRequest.getAttribute("javax.portlet.portletc.portletWindowName");
	}
	
	public String retrieveServiceName(RenderRequest renderRequest, RenderResponse renderResponse) {
		return (String) renderRequest.getAttribute("javax.portlet.portletc.portletName");
	}
}

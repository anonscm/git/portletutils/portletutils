package org.evolvis.portal.portletutils.portletregistry.liferay;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * There are differences in how portals implemenent the retrieval 
 * of portlet name and portletId. Implement this interface for your
 * portal server.
 */
public interface PortletDataProvider {

	/**
	 * Retrieves portlet id for a portlet
	 */
	public String retrievePortletId(RenderRequest renderRequest, RenderResponse renderResponse);

	/**
	 * Retrieves default service name (external identifier) for a portlet
	 */
	public String retrieveServiceName(RenderRequest renderRequest, RenderResponse renderResponse);
	
}

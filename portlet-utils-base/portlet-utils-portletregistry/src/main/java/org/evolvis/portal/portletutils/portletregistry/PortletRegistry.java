/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry;

import java.util.List;

import javax.portlet.PortletURL;

import org.evolvis.portal.portletutils.portletregistry.exception.PortletRegistryException;
import org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException;

/**
 * The Interface PortletRegistry.
 * 
 * @author pcorne
 */
public interface PortletRegistry {
	
	/**
	 * Register portlet instance.
	 * 
	 * When there is already a portlet registered with the same servicename, the
	 * data will be updated to the new values
	 * 
	 * @param newPortletRegistryItem the new portlet registry item
	 */
	void registerPortletInstance(PortletRegistryItem newPortletRegistryItem);

	/**
	 * Unregister PortletRegistryItem for concrete itemId.
	 */
	void unregisterItem(Long itemId);

	/**
	 * Unregister portlet instance.
	 * 
	 * This removes the portlet entries for all servicenames it was registered
	 * for. Call this only when the portlet it about to get removed not when the
	 * class is removed or the server is shut down!
	 * 
	 * @param portletId the portlet id
	 */
	void unregisterPortletInstance(String portletId);

	/**
	 * Creates the action url for a portlet with the given servicename.
	 * 
	 * @param servicename the servicename
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createActionUrl(String servicename) throws PortletRegistryException;
	
	/**
	 * Creates the action url for a portlet with the given servicename.
	 * 
	 * @param servicename the servicename
	 * @param context the context
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createActionUrl(String servicename,String context) throws PortletRegistryException;

	/**
	 * Creates the action url.
	 * 
	 * @param item the item
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException the portlet registry exception
	 */
	PortletURL createActionUrl(PortletRegistryItem item) throws PortletRegistryException;
	
	/**
	 * Creates the render url.
	 * 
	 * @param servicename the servicename
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createRenderUrl(String servicename) throws PortletRegistryException;
	
	/**
	 * Creates the render url.
	 * 
	 * @param servicename the servicename
	 * @param context the context
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createRenderUrl(String servicename,String context) throws PortletRegistryException;
	
	/**
	 * Creates the render url.
	 * 
	 * @param item the item
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException the portlet registry exception
	 */
	PortletURL createRenderUrl(PortletRegistryItem item) throws PortletRegistryException;
	
	/**
	 * Creates the resource url.
	 * 
	 * @param servicename the servicename
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createResourceUrl(String servicename) throws PortletRegistryException;
	
	/**
	 * Creates the resource url.
	 * 
	 * @param servicename the servicename
	 * @param context the context
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException when the url could not be build for example because no
	 * PortletURLProvider is present
	 * @throws ServiceUnknownException when no portlet has been registered for this service
	 */
	PortletURL createResourceUrl(String servicename,String context) throws PortletRegistryException;

	/**
	 * Creates the resource url.
	 * 
	 * @param item the item
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException the portlet registry exception
	 */
	PortletURL createResourceUrl(PortletRegistryItem item) throws PortletRegistryException;
	
	/**
	 * Sets the portlet url provider implementation.
	 * 
	 * @param portletURLProvider the new portlet url provider
	 */
	void setPortletURLProvider(PortletURLProvider portletURLProvider);
	
	/**
	 * Sets the Portal context provider implementation.
	 * 
	 * @param portalContextProvider the new Portal context provider
	 */
	void setPortalContextProvider(PortalContextProvider newPortalContextProvider);
	
	/**
	 * Creates the registry item.
	 * 
	 * @param baseUrl the base url
	 * @param portletId the portlet id
	 * @param serviceName the service name
	 * @param context the context
	 * 
	 * @return the portlet registry item
	 */
	PortletRegistryItem createRegistryItem(String baseUrl, String portletId, String serviceName,String context);

	/**
	 * Gets the registry item for service.
	 * 
	 * @param serviceName the service name
	 * 
	 * @return the registry item for service
	 */
	PortletRegistryItem getRegistryItemForService(String serviceName);
	
	/**
	 * Gets the registry item for service and context.
	 * 
	 * The lookup strategy used by this method depends on whether the given context is set or not.
	 * <ul>
	 * <li>If the given context is <code>null</code>, the registry will look for any portlet
	 * with a matching service name.</li>
	 * <li><If the given context is not <code>null</code>, the registry will only search the given context.</li>
	 * </ul>
	 * 
	 * @param serviceName the service name
	 * @param context the context in which to look.
	 * 
	 * @return a matching registry item for the given service and context, or <code>null</code> if non was registered.
	 */
	PortletRegistryItem getRegistryItemForService(String serviceName, String context);
	
	/**
	 * Gets all registry items.
	 * 
	 * @return the items
	 */
	List<? extends PortletRegistryItem> getAllRegistryItems();
	
	
	/**
	 * Gets all registry items for a given service name.
	 * 
	 * @return the items
	 */
	List<? extends PortletRegistryItem> getAllRegistryItemsForService(String serviceName);
	
	/**
	 * Gets the all registry items for a given context.
	 * 
	 * @return the items
	 */
	List<? extends PortletRegistryItem> getAllRegistryItemsForContext(String context);
	
	/**
	 * Gets the all registry items for a given portlet id.
	 * 
	 * @return the items
	 */
	List<? extends PortletRegistryItem> getAllRegistryItemsForPortletId(String portletId);

	/**
	 * Obtain a regestry entry by its id.
	 * 
	 * @param registryItemId The id to look up.  
	 * @return
	 */
	PortletRegistryItem getRegistryItemById(Long registryItemId);
}

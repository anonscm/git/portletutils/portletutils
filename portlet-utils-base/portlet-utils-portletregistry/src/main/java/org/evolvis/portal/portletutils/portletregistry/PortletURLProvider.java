/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry;

import javax.portlet.PortletURL;

/**
 * The Interface PortletURLProvider.
 * 
 * @author pcorne
 */
public interface PortletURLProvider {

	/**
	 * Creates the action url.
	 * 
	 * @param portletId
	 *            the portlet id
	 * 
	 * @return the portlet url
	 */
	PortletURL createActionUrl(String baseUrl, String portletId);

	/**
	 * Creates the render url.
	 * 
	 * @param portletId
	 *            the portlet id
	 * 
	 * @return the portlet url
	 */
	PortletURL createRenderUrl(String baseUrl, String portletId);

	/**
	 * Creates the resource url.
	 * 
	 * @param portletId
	 *            the portlet id
	 * 
	 * @return the portlet url
	 */
	PortletURL createResourceUrl(String baseUrl, String portletId);
}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.util;

import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;

/**
 * @author pcorne
 * 
 */
public class URLUtil {

	private URLUtil() {

	}

	public static String extractBaseURL(String fullURL) {
		return fullURL.split("(\\?|;)")[0];
	}
	
	public static String retrieveBaseURL(RenderResponse response) {
		PortletURL portletURL = response.createRenderURL();
		return extractBaseURL(portletURL.toString());
	}
}

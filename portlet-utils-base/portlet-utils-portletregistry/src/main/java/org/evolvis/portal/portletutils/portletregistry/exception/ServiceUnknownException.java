/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.exception;

/**
 * The Class PortletRegistryServiceUnknownException.
 * 
 * @author pcorne
 */
public class ServiceUnknownException extends
		PortletRegistryException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3566260773513526174L;
	private String name;
	private String context;

	/**
	 * Instantiates a new portlet registry service unknown exception.
	 */
	public ServiceUnknownException(String name, String context) {
		super("error.portletregistry.service_unknown","No portlet registered for the given service");
		this.name=name;
		this.context=context;
	}

	/**
	 * Instantiates a new portlet registry service unknown exception.
	 * 
	 * @param msg
	 *            the msg
	 */
	ServiceUnknownException(String name, String context,String msg) {
		super("error.portletregistry.service_unknown",msg);
		this.name=name;
		this.context=context;
	}

	@Override
	public Object[] getArguments() {
		// TODO Auto-generated method stub
		return new Object[]{name,context};
	}
}

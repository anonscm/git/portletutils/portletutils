/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry;


/**
 * @author pcorne
 *
 */


public interface PortletRegistryItem {

	/**
	 * Gets the base url.
	 * 
	 * @return the baseUrl
	 */	
	public String getBaseUrl();

	/**
	 * Sets the base url.
	 * 
	 * @param baseUrl
	 *            the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl);

	/**
	 * Gets the portlet id.
	 * 
	 * @return the portletId
	 */
	public String getPortletId();

	/**
	 * Sets the portlet id.
	 * 
	 * @param portletId
	 *            the portletId to set
	 */
	public void setPortletId(String portletId);

	/**
	 * Sets the service name.
	 * 
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName);

	/**
	 * Gets the service name.
	 * 
	 * @return the serviceName
	 */
	public String getServiceName();
	
	public void setContext(String context);
	
	
	public String getContext();
	public Long getId();

}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.exception;

import org.evolvis.exceptions.base.AbstractBaseException;

/**
 * The Class PortletRegistryException.
 * 
 * @author pcorne
 */
public class PortletRegistryException extends AbstractBaseException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6713827056667373751L;

	/**
	 * Instantiates a new portlet registry exception.
	 * 
	 * @param msg
	 *            the msg	
	 */
	
	public PortletRegistryException(String code,String msg) {
		super(code,msg);
	}

	/**
	 * Instantiates a new portlet registry exception.
	 */
	@Deprecated
	public PortletRegistryException() {
		this("error.portletregestry","unknown");
	}
	
	
	
}

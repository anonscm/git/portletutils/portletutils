package org.evolvis.portal.portletutils.portletregistry;

/**
 * This interface can be implemented by a Portal specific implementation of the
 * Portlet Registry to retrieve a service context from the Portal.
 * 
 * This may be necessary if the Portal provides the same Portlet in different
 * contexts by design. E.g. the Liferay Portal may provide several Portal
 * instances which Portlets are registered with the same service name.
 * 
 * On retrieval of a Portlet Registry item typically the Portlet in the same
 * Portal instance is requested. If no explicit context is requested a Portal
 * context provider may help to retrieve the typical context.
 * 
 * A Portal context provider implementation is not mandatory for a Portal
 * implementation of the Portlet Registry.
 * 
 * @author Jens Neumaier, tarent GmbH
 */
public interface PortalContextProvider {

	public String retrievePortalContext();

}

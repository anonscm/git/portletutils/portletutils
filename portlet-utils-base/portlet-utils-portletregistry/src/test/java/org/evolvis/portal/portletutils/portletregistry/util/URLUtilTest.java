/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.util;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author pcorne
 * 
 */
public class URLUtilTest {
	@Test
	public void testExtractBaseURL() {
		assertEquals("http://test.me/hier/da", URLUtil
				.extractBaseURL("http://test.me/hier/da?param=value&here=not"));
		assertEquals("http://test.me/hier/da", URLUtil
				.extractBaseURL("http://test.me/hier/da;jsessionid"));
		assertEquals("http://test.me/hier/da", URLUtil
				.extractBaseURL("http://test.me/hier/da;jsessionid?param=value"));
		assertEquals("http://test.me/hier/da", URLUtil
				.extractBaseURL("http://test.me/hier/da?param=value;jsessionid"));
		Assert.assertFalse("http://test.me/hier/da".equals(URLUtil
				.extractBaseURL("http://test.me/hier/da2?param=value;jsessionid")));
	}
}

/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException;
import org.junit.Test;

/**
 * @author pcorne
 * 
 */
public class ServiceUnknownExceptionTest {

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException#ServiceUnknownException()}
	 * .
	 */
	@Test
	public void testServiceUnknownException() {
		try {
			// test correct inheritance ;)
			throw new ServiceUnknownException("name","context");
		} catch (PortletRegistryException e) {
			assertTrue(true);
		}
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException#ServiceUnknownException(java.lang.String)}
	 * .
	 */
	@Test
	public void testServiceUnknownExceptionString() {
		try {
			// test correct inheritance ;)
			throw new ServiceUnknownException("name","context","bla");
		} catch (ServiceUnknownException e) {
			assertEquals("bla", e.getMessage());
		}
	}

}

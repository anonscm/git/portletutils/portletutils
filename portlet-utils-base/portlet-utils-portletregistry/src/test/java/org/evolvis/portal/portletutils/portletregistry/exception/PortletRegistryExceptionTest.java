/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.exception;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * @author pcorne
 * 
 */
public class PortletRegistryExceptionTest {

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.PortletRegistryException#PortletRegistryException(java.lang.String)}
	 * .
	 */
	@Test
	public void testPortletRegistryExceptionString() {
		try {
			// test correct inheritance ;)
			throw new PortletRegistryException("error.portletregestry","bla");
		} catch (PortletRegistryException e) {
			assertEquals("bla", e.getMessage());
		}
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.PortletRegistryException#PortletRegistryException()}
	 * .
	 */
	@Test
	public void testPortletRegistryException() {
		try {
			// test correct inheritance ;)
			throw new PortletRegistryException();
		} catch (Exception e) {
			assertTrue(true);
		}
	}

}

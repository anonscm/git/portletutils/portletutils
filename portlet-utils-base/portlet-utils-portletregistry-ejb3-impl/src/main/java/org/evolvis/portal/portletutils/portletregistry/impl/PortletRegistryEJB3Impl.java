/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.portlet.PortletURL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.evolvis.config.Configuration;
import org.evolvis.config.PropertyConfig;
import org.evolvis.portal.portletutils.portletregistry.PortalContextProvider;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryService;
import org.evolvis.portal.portletutils.portletregistry.PortletURLProvider;
import org.evolvis.portal.portletutils.portletregistry.exception.PortletRegistryException;
import org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException;

/**
 * The Class PortletRegistryEJB3Impl.
 */
@Stateless
@Local(PortletRegistry.class)
public class PortletRegistryEJB3Impl implements PortletRegistry {
	
	private static Log log = LogFactory.getLog(PortletRegistryEJB3Impl.class.getName());

	/** The portlet url provider. */
	private PortletURLProvider portletURLProvider;

	private PortletRegistryService portletRegistryService;
	
	private PortalContextProvider portalContextProvider;
	
	private String containerType = null;

	public PortletRegistryEJB3Impl() {
		Configuration config = new PropertyConfig();
		// load from portal-tarent.properties, default value is 'sun'
		containerType = config.get("portletregistry.container.impl", "sun");
		// try to load a PortalContextProvider after into portlet registry service
		try {
			String urlProviderClass = null;
			String contextProviderClass = null;
			if ("sun".equals(containerType)) {
				urlProviderClass = org.evolvis.portal.portletutils.portletregistry.sun.PortletURLProviderImpl.class.getName();
				contextProviderClass = org.evolvis.portal.portletutils.portletregistry.sun.PortalContextProviderImpl.class.getName();
			} else if ("liferay".equals(containerType)) {
				urlProviderClass = org.evolvis.portal.portletutils.portletregistry.liferay.PortletURLProviderImpl.class.getName();
				contextProviderClass = org.evolvis.portal.portletutils.portletregistry.liferay.PortalContextProviderImpl.class.getName();
			} else {
				throw new IllegalArgumentException("Invalid container type given. Valid values are either 'sun' or 'liferay'.");
			}
			log.info("Using PortletURLProvider: '" + urlProviderClass + "' and ContextProvider '" + contextProviderClass + "'.");

			setPortalContextProvider(((Class<PortalContextProvider>) Class.forName(contextProviderClass)).newInstance());
			setPortletURLProvider(((Class<PortletURLProvider>) Class.forName(urlProviderClass)).newInstance());
		} catch (final Exception e) {
			log.error("Could not instantiate", e);
		}
		
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws PortletRegistryException
	 */
	public PortletURL createActionUrl(String servicename)
			throws PortletRegistryException {
		return createUrl(servicename,null, URLType.action);
	}
	/**
	 * {@inheritDoc}
	 * 
	 * @throws PortletRegistryException
	 */
	public PortletURL createActionUrl(String servicename,String context)
			throws PortletRegistryException {
		return createUrl(servicename,context, URLType.action);
	}

	/** {@inheritDoc} */
	public PortletURL createRenderUrl(String servicename)
			throws PortletRegistryException {
		return createUrl(servicename,null, URLType.render);
	}
	
	/** {@inheritDoc} */
	public PortletURL createRenderUrl(String servicename,String context)
			throws PortletRegistryException {
		return createUrl(servicename,context, URLType.render);
	}

	/** {@inheritDoc} */
	public PortletURL createResourceUrl(String servicename)
			throws PortletRegistryException {
		return createUrl(servicename,null, URLType.resource);
	}
	
	/** {@inheritDoc} */
	public PortletURL createResourceUrl(String servicename, String context)
			throws PortletRegistryException {
		return createUrl(servicename,context, URLType.resource);
	}

	/**
	 * The Enum URLType.
	 */
	private enum URLType {

		/** The render. */
		render,
		/** The action. */
		action,
		/** The resource. */
		resource
	}

	/**
	 * Creates the url.
	 * 
	 * @param serviceName
	 *            the servicename
	 * @param type
	 *            the type
	 * 
	 * @return the portlet url
	 * 
	 * @throws PortletRegistryException
	 *             the portlet itemsByServiceName exception
	 * @throws ServiceUnknownException
	 *             the service unknown exception
	 */
	private PortletURL createUrl(String serviceName, String context, URLType type)
			throws PortletRegistryException, ServiceUnknownException {
		if (portletURLProvider == null) {
			throw new PortletRegistryException("error.portletregestry.missing_url_provider", "missing url provider");
		}
		PortletRegistryItem item = portletRegistryService
				.retrievePortletByServiceName(serviceName, context);
		// System.out.println("Create " + type + " Url for: " + item);
		if (item == null) {
			throw new ServiceUnknownException(serviceName, context);
		}
		PortletURL result = createUrl(type, item);

		return result;
	}

	private PortletURL createUrl(URLType type, PortletRegistryItem item) {
		// used local variable instead of return to keep the cyclomatic
		// complexity low
		PortletURL result = null;
		switch (type) {
		case action:
			result = portletURLProvider.createActionUrl(item.getBaseUrl(), item
					.getPortletId());
			break;
		case render:
			result = portletURLProvider.createRenderUrl(item.getBaseUrl(), item
					.getPortletId());
			break;
		case resource:
			result = portletURLProvider.createResourceUrl(item.getBaseUrl(),
					item.getPortletId());
			break;
		}
		return result;
	}

	/** {@inheritDoc} */
	public void setPortletURLProvider(PortletURLProvider newPortletURLProvider) {
		this.portletURLProvider = newPortletURLProvider;
	}

	/** {@inheritDoc} */
	public void setPortalContextProvider(PortalContextProvider newPortalContextProvider) {
		this.portalContextProvider = newPortalContextProvider;
	}

	/**
	 * @param portletRegistryService
	 *            the portletRegistryService to set
	 */
	@EJB
	public void setPortletRegistryService(
			PortletRegistryService portletRegistryService) {
		this.portletRegistryService = portletRegistryService;
	}

	/** {@inheritDoc} */
	public void registerPortletInstance(
			PortletRegistryItem newPortletRegistryItem) {
		if (newPortletRegistryItem.getContext() == null && portalContextProvider != null) {
			/*
			 * Try to load default context from {@link PortalContextProvider}.
			 */
			newPortletRegistryItem.setContext(portalContextProvider.retrievePortalContext());
		}
		
		portletRegistryService
				.savePortletRegistryItem((PortletRegistryItemEntity) newPortletRegistryItem);
		log.info("Successfully registered portlet with serviceName=" + newPortletRegistryItem.getServiceName() + ", context=" + newPortletRegistryItem.getContext() + ", baseUrl=" + newPortletRegistryItem.getBaseUrl() + ", portletId="
				+ newPortletRegistryItem.getPortletId() + ", context=" + newPortletRegistryItem.getContext() + " at the Portlet Registry.");
	}

	/** {@inheritDoc} */
	public void unregisterItem(Long itemId) {
		portletRegistryService.deleteItem(itemId);
	}

	/** {@inheritDoc} */
	public void unregisterPortletInstance(String portletId) {
		portletRegistryService.deletePortlet(portletId);
	}

	/** {@inheritDoc} */
	public PortletRegistryItem createRegistryItem(String baseUrl,
			String portletId, String serviceName, String context) {
		return new PortletRegistryItemEntity(baseUrl, portletId, serviceName,context);
	}

	/** {@inheritDoc} */
	public PortletRegistryItem getRegistryItemForService(String serviceName) {
		return portletRegistryService.retrievePortletByServiceName(serviceName,null);
	}

	
	public PortletRegistryItem getRegistryItemForService(String serviceName,
			String context) {
		if (context == null && portalContextProvider != null) {
			/*
			 * Try to load default context from {@link PortalContextProvider}.
			 */
			context = portalContextProvider.retrievePortalContext();
		}
		
		return portletRegistryService.retrievePortletByServiceName(serviceName,context);
	}

	public List<? extends PortletRegistryItem> getAllRegistryItems() {
		
		return portletRegistryService.retrieveAllEntries();
	}

	public List<? extends PortletRegistryItem> getAllRegistryItemsForContext(String context) {
		return portletRegistryService.retrieveAllEntriesForContext(context);
	}

	public List<? extends PortletRegistryItem> getAllRegistryItemsForService(String serviceName) {

		return portletRegistryService.retrieveAllEntriesForServiceName(serviceName);
	}

	public List<? extends PortletRegistryItem> getAllRegistryItemsForPortletId(
			String portletId) {	
		return portletRegistryService.retrieveAllEntriesForPortletId(portletId);
	}
	
	public PortletRegistryItem getRegistryItemById(Long registryItemId) {
		
		return portletRegistryService.getRegistryItemById(registryItemId);
	}
	
	public PortletURL createActionUrl(PortletRegistryItem item)
			throws PortletRegistryException {
		return createUrl(URLType.action, item);
	}

	public PortletURL createRenderUrl(PortletRegistryItem item)
			throws PortletRegistryException {
		return createUrl(URLType.render, item);
	}

	public PortletURL createResourceUrl(PortletRegistryItem item)
			throws PortletRegistryException {
		return createUrl(URLType.resource, item);
	}

}

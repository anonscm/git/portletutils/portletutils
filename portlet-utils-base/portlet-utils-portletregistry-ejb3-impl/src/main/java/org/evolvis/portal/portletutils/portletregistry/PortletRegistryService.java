/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry;

import java.util.List;

import org.evolvis.portal.portletutils.portletregistry.impl.PortletRegistryItemEntity;

/**
 * The Interface PortletRegistryService.
 * 
 * @author pcorne
 */
public interface PortletRegistryService {

	/**
	 * Retrieve portlet by service name.
	 * 
	 * @param serviceName
	 *            the service name
	 * 
	 * @return the portlet itemsByServiceName item
	 */
	PortletRegistryItemEntity retrievePortletByServiceName(final String serviceName, final String context);

	/**
	 * Save portlet itemsByServiceName item.
	 * 
	 * @param item
	 *            the item
	 */
	void savePortletRegistryItem(final PortletRegistryItemEntity item);

	/**
	 * Delete portlet by portletId
	 * @param portletId  the portlet id
	 */
	void deletePortlet(final String portletId);

	/**
	 * Delete portlet by itemId
	 * @param itemId if od the item
	 */
	void deleteItem(final Long itemId);
	
	List<? extends PortletRegistryItem> retrieveAllEntries();

	List<? extends PortletRegistryItem> retrieveAllEntriesForContext(String context);
	
	List<? extends PortletRegistryItem> retrieveAllEntriesForServiceName(String serviceName);

	List<? extends PortletRegistryItem> retrieveAllEntriesForPortletId(String portletId);

	PortletRegistryItem getRegistryItemById(Long registryItemId);
}

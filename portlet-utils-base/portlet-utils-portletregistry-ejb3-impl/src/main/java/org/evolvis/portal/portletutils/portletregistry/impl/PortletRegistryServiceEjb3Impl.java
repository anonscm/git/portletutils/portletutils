/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryService;

/**
 * @author pcorne
 * @author Tino Rink
 */
@Stateless
@Local(PortletRegistryService.class)
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class PortletRegistryServiceEjb3Impl implements PortletRegistryService {
	
	@PersistenceContext(unitName = "portletregistry")
	EntityManager manager;
	
	public void deleteItem(final Long itemId) {
		Query query = manager.createQuery("delete from PortletRegistryItemEntity item where item.id = :itemId");
		query.setParameter("itemId", itemId);
		query.executeUpdate();
	}
	
	public void deletePortlet(final String portletId) {
		Query query = manager.createQuery("delete from PortletRegistryItemEntity item where item.portletId = :portletid");
		query.setParameter("portletid", portletId);
		query.executeUpdate();
	}

	public PortletRegistryItemEntity retrievePortletByServiceName(
			String serviceName, String context) {

		Query query;
		if(context==null){
			query = manager.createQuery("from PortletRegistryItemEntity i where serviceName=:service");
			query.setParameter("service", serviceName);
		}else{
			query = manager
			.createQuery("from PortletRegistryItemEntity i where serviceName=:service and context=:context");
			query.setParameter("service", serviceName);
			query.setParameter("context", context);
		}
		try {
			List resultList = query.getResultList();
			if(resultList==null||resultList.isEmpty()){
				return null;
			}
			return (PortletRegistryItemEntity) resultList.get(0);
		} catch (NoResultException e) {
			return null;
		}
	}

	public void savePortletRegistryItem(final PortletRegistryItemEntity item) {
		
		/*
		 * update persistent registry item if present
		 */
		PortletRegistryItemEntity existingEntry = null;
		if (item.getId() != null) {
			existingEntry = manager.find(PortletRegistryItemEntity.class, item.getId());
		}
		
		/*
		 * make sure that there is no other item with the same servicename and context
		 */
		if (existingEntry == null) {
			existingEntry = retrievePortletByServiceName(item
				.getServiceName(),item.getContext());
		}
		
		if (existingEntry != null) {
			item.setId(existingEntry.getId());
			manager.merge(item);
		} else {
			manager.persist(item);
		}
	}

	public List<PortletRegistryItem> retrieveAllEntries() {
		manager.find(PortletRegistryItemEntity.class, 1L);
		Query query;
		query = manager.createQuery("from PortletRegistryItemEntity");				
		try {
			List<PortletRegistryItem> resultList = query.getResultList();			
			return resultList;
		} catch (NoResultException e) {
			return new ArrayList<PortletRegistryItem>();
		}
	}

	public List<PortletRegistryItem> retrieveAllEntriesForContext(String context) {
		manager.find(PortletRegistryItemEntity.class, 1L);
		Query query;
		query = manager.createQuery("from PortletRegistryItemEntity i where context=:context");	
		query.setParameter("context", context);
		try {
			List<PortletRegistryItem> resultList = query.getResultList();			
			return resultList;
		} catch (NoResultException e) {
			return new ArrayList<PortletRegistryItem>();
		}
	}

	public List<PortletRegistryItem> retrieveAllEntriesForServiceName(
			String serviceName) {
		manager.find(PortletRegistryItemEntity.class, 1L);
		
		Query query;
		query = manager.createQuery("from PortletRegistryItemEntity i where serviceName=:service");	
		query.setParameter("service", serviceName);
		try {
			List<PortletRegistryItem> resultList = query.getResultList();			
			return resultList;
		} catch (NoResultException e) {
			return new ArrayList<PortletRegistryItem>();
		}
	}

	public List<? extends PortletRegistryItem> retrieveAllEntriesForPortletId(
			String portletId) {
		manager.find(PortletRegistryItemEntity.class, 1L);
		Query query;
		query = manager.createQuery("from PortletRegistryItemEntity i where portletId=:portletid");	
		query.setParameter("portletid", portletId);
		try {
			List<PortletRegistryItem> resultList = query.getResultList();			
			return resultList;
		} catch (NoResultException e) {
			return new ArrayList<PortletRegistryItem>();
		}
	}

	public PortletRegistryItem getRegistryItemById(Long registryItemId) {
		return registryItemId==null? null : manager.find(PortletRegistryItemEntity.class, registryItemId);
	}
}

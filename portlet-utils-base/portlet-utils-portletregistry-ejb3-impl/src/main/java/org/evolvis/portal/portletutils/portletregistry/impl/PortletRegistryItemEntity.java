/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;

/**
 * The Class PortletRegistryItem.
 * 
 * @author pcorne
 */
@Entity
@Table(name = "portletregistryitem" ,uniqueConstraints={@UniqueConstraint(columnNames={"SERVICENAME","CONTEXT"})})
@SequenceGenerator(name = "portletregistryitem_seq", sequenceName = "portletregistryitem_id_seq")
public class PortletRegistryItemEntity implements PortletRegistryItem,
		Serializable {
	private static final long serialVersionUID = -9052126062594931553L;
	private Long id;
	/** The base url. */
	private String baseUrl;

	/** The portlet id. */
	private String portletId;

	/** The service name. */
	private String serviceName;
	private String context;

	public PortletRegistryItemEntity() {
		super();
	}

	

	
	
	public PortletRegistryItemEntity(String newBaseUrl, String newPortletId,
			String newServiceName,String context) {
		super();
		id = null;
		setContext(context);
		setBaseUrl(newBaseUrl);
		setPortletId(newPortletId);
		setServiceName(newServiceName);
	}

	public PortletRegistryItemEntity(Long newId, String newBaseUrl,
			String newPortletId, String newServiceName,String context) {
		super();
		setContext(context);
		setId(newId);
		setBaseUrl(newBaseUrl);
		setPortletId(newPortletId);
		setServiceName(newServiceName);
	}

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "portletregistryitem_seq")
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Gets the base url.
	 * 
	 * @return the baseUrl
	 */
	@Column(nullable = false, length = 255)
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * Sets the base url.
	 * 
	 * @param baseUrl
	 *            the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * Gets the portlet id.
	 * 
	 * @return the portletId
	 */
	@Column(nullable = false, length = 200)
	public String getPortletId() {
		return portletId;
	}

	/**
	 * Sets the portlet id.
	 * 
	 * @param portletId
	 *            the portletId to set
	 */
	public void setPortletId(String portletId) {
		this.portletId = portletId;
	}

	/**
	 * Sets the service name.
	 * 
	 * @param serviceName
	 *            the serviceName to set
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * Gets the service name.
	 * 
	 * @return the serviceName
	 */
	@Column(nullable = false, unique = false, length = 200)
	public String getServiceName() {
		return serviceName;
	}

	/** {@inheritDoc} */
	@Override
	public boolean equals(Object obj) {
		EqualsBuilder builder = new EqualsBuilder();
		PortletRegistryItem item = (PortletRegistryItem) obj;
		builder.append(serviceName, item.getServiceName());
		builder.append(portletId, item.getPortletId());
		return builder.isEquals();
	}

	/** {@inheritDoc} */
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(317, 1111);
		builder.append(serviceName);
		builder.append(portletId);
		return builder.toHashCode();
	}

	/** {@inheritDoc} */
	@Override
	public String toString() {
		return "baseURL: " + baseUrl + " portletId: " + portletId
				+ " serviceName: " + serviceName;
	}

	@Column(nullable = false, length = 200)
	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context=context;		
	}
}

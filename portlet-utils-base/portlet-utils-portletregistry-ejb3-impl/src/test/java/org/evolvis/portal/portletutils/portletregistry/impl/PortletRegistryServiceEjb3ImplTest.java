/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;

/**
 * @author pcorne
 * 
 */
public class PortletRegistryServiceEjb3ImplTest extends
		BaseSessionBeanFixture<PortletRegistryServiceEjb3Impl> {
	private static final Class<?>[] usedBeans = { PortletRegistryItemEntity.class };

	public PortletRegistryServiceEjb3ImplTest() {
		super(PortletRegistryServiceEjb3Impl.class, usedBeans);
	}

	private PortletRegistryServiceEjb3Impl service;

	/**
	 * @throws java.lang.Exception
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		service = getBeanToTest();
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryServiceEjb3Impl#savePortletRegistryItem(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 */
	@Test
	public void testSavePortletRegistryItem() {
		final String baseUrl = "baseurl";
		final String portletId = "portletid";
		final String serviceName = "servicename";
		final String context= "context";
		// ejb3unit doesn't support transaction management yet, we do it on your
		// own
		EntityTransaction transaction = getEntityManager().getTransaction();
		transaction.begin();
		service.savePortletRegistryItem(new PortletRegistryItemEntity(baseUrl,
				portletId, serviceName,context));
		try {
			Query query = getEntityManager().createQuery(
					"from PortletRegistryItemEntity i where baseURL=:baseurl"
							+ " and portletId=:portletid"
							+ " and serviceName=:servicename");
			query.setParameter("baseurl", baseUrl);
			query.setParameter("portletid", portletId);
			query.setParameter("servicename", serviceName);
			query.getSingleResult();
		} catch (NoResultException e) {
			fail(e.getMessage());
		} finally {
			transaction.commit();
		}
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryServiceEjb3Impl#retrievePortletByServiceName(java.lang.String)}
	 * .
	 */
	@Test
	public void testRetrievePortletByServiceName() {
		service.savePortletRegistryItem(new PortletRegistryItemEntity(
				"baseurl", "portletid", "servicename","context"));

		assertNotNull("portletid", service
				.retrievePortletByServiceName("servicename","context"));
		assertEquals("portletid", service.retrievePortletByServiceName(
				"servicename","context").getPortletId());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryServiceEjb3Impl#deletePortlet(java.lang.String)}
	 * .
	 */
	@Test
	public void testDeletePortlet() {
		// ejb3unit doesn't support transaction management yet, we do it on your
		// own
		EntityTransaction transaction = getEntityManager().getTransaction();
		transaction.begin();
		service.savePortletRegistryItem(new PortletRegistryItemEntity(
				"baseurl", "portletid", "servicename","context"));
		service.deletePortlet("portletid");
		assertNull(service.retrievePortletByServiceName("servicename","context"));
		transaction.commit();

	}

}

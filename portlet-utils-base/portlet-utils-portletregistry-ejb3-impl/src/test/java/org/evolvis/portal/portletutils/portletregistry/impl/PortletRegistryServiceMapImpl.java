/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.evolvis.portal.portletutils.portletregistry.PortalContextProvider;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryItem;
import org.evolvis.portal.portletutils.portletregistry.PortletRegistryService;

/**
 * @author pcorne
 * 
 */
public class PortletRegistryServiceMapImpl implements PortletRegistryService {
	Map<String, ArrayList<PortletRegistryItemEntity>> itemsByServiceName = new HashMap<String, ArrayList<PortletRegistryItemEntity>>();
	Map<String, PortletRegistryItemEntity> itemsByQualifiedName = new HashMap<String, PortletRegistryItemEntity>();
	
	/** {@inheritDoc} */
	public void deletePortlet(String portletId) {
		for(ArrayList<PortletRegistryItemEntity> list:itemsByServiceName.values()){
			for(int i=list.size();i>0;){
				PortletRegistryItemEntity item = list.get(--i);
				if(item.getPortletId().equals(portletId)){
					list.remove(i);
				}
			}
		}		
		HashSet<String> keySet = new HashSet<String>(itemsByQualifiedName.keySet());
		for (String key : keySet) {
			if (itemsByQualifiedName.get(key).getPortletId().equals(portletId)) {
				itemsByQualifiedName.remove(key);
			}
		}
	}

	public void deleteItem(Long itemId) {
		// TODO Auto-generated method stub
	}
	
	/** {@inheritDoc} */
	public PortletRegistryItemEntity retrievePortletByServiceName(String serviceName,String context) {
		if(context==null){
			ArrayList<PortletRegistryItemEntity> list = itemsByServiceName.get(serviceName);
			if(list==null||list.isEmpty()){
				return null;
			}
			return list.get(0);
		}
		return itemsByQualifiedName.get(context+"."+serviceName);
	}

	/** {@inheritDoc} */
	public void savePortletRegistryItem(PortletRegistryItemEntity item) {
		ArrayList<PortletRegistryItemEntity> list = itemsByServiceName.get(item.getServiceName());
		if(list==null){
			list=new ArrayList<PortletRegistryItemEntity>();
			itemsByServiceName.put(item.getServiceName(), list);
		}
		if(!list.contains(item)){
			list.add(item);
		}
		itemsByQualifiedName.put(item.getContext()+"."+item.getServiceName(), item);
	}

	public List<PortletRegistryItem> retrieveAllEntries() {
		ArrayList<PortletRegistryItem> result = new ArrayList<PortletRegistryItem>();
		result.addAll(itemsByQualifiedName.values());
		return result;
	}

	public List<PortletRegistryItem> retrieveAllEntriesForContext(String context) {
		ArrayList<PortletRegistryItem> result = new ArrayList<PortletRegistryItem>();
		for(PortletRegistryItem item:itemsByQualifiedName.values()){
			if(item.getContext().equals(context)){
				result.add(item);
			}
		}
		return result;
	}

	public List<? extends PortletRegistryItem> retrieveAllEntriesForServiceName(
			String serviceName) {

		return itemsByServiceName.get(serviceName);
	}

	public List<? extends PortletRegistryItem> retrieveAllEntriesForPortletId(
			String portletId) {
		ArrayList<PortletRegistryItem> result = new ArrayList<PortletRegistryItem>();
		for(PortletRegistryItem item:itemsByQualifiedName.values()){
			if(item.getPortletId().equals(portletId)){
				result.add(item);
			}
		}
		return result;
	}

	public PortletRegistryItem getRegistryItemById(Long registryItemId) {
		for(PortletRegistryItem item:itemsByQualifiedName.values()){
			if(item.getId().equals(registryItemId)){
				return item;
			}
		}
		return null;
	}

	public void setPortalContextProvider(PortalContextProvider newPortalContextProvider) {
	}

}

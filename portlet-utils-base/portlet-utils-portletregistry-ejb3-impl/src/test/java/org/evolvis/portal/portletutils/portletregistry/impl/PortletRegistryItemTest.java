/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;

/**
 * @author pcorne
 * 
 */
public class PortletRegistryItemTest {
	private PortletRegistryItemEntity item;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		item = new PortletRegistryItemEntity();
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#getBaseUrl()}
	 * .
	 */
	@Test
	public void testGetBaseUrl() {
		String testUrl = "http://test.me:9988/now";
		assertFalse(testUrl.equals(item.getBaseUrl()));
		item.setBaseUrl(testUrl);
		assertEquals(testUrl, item.getBaseUrl());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#getPortletId()}
	 * .
	 */
	@Test
	public void testGetPortletId() {
		String testId = "i_am_a_portletid";
		assertFalse(testId.equals(item.getPortletId()));
		item.setPortletId(testId);
		assertEquals(testId, item.getPortletId());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#getServiceName()}
	 * .
	 */
	@Test
	public void testGetServiceName() {
		String testName = "42-provider";
		assertFalse(testName.equals(item.getServiceName()));
		item.setServiceName(testName);
		assertEquals(testName, item.getServiceName());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#getId()}
	 * .
	 */
	@Test
	public void testGetId() {
		Long id = Long.valueOf(23);
		assertFalse(id.equals(item.getId()));
		item.setId(id);
		assertEquals(id, item.getId());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#PortletRegistryItem(String, String, String)}
	 * .
	 */
	@Test
	public void testPortletRegistryItem() {
		String testServiceName = "23-conspiracy";
		String testPortletId = "super-portlet";
		String testBaseUrl = "http://host.dot:1234/works";
		String testContext = "uber-context";
		item = new PortletRegistryItemEntity(testBaseUrl, testPortletId,
				testServiceName,testContext);
		assertNull(item.getId());
		assertEquals(testServiceName, item.getServiceName());
		assertEquals(testBaseUrl, item.getBaseUrl());
		assertEquals(testPortletId, item.getPortletId());
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity#PortletRegistryItem(String, String, String)}
	 * .
	 */
	@Test
	public void testPortletRegistryItemWithId() {
		Long id = Long.valueOf(23);
		String testServiceName = "23-conspiracy";
		String testPortletId = "super-portlet";
		String testBaseUrl = "http://host.dot:1234/works";
		String testContext= "uber-context";
		item = new PortletRegistryItemEntity(id, testBaseUrl, testPortletId,
				testServiceName,testContext);
		assertEquals(id, item.getId());
		assertEquals(testServiceName, item.getServiceName());
		assertEquals(testBaseUrl, item.getBaseUrl());
		assertEquals(testPortletId, item.getPortletId());
	}

	@Test
	public void testSerializable() {
		assertTrue(Serializable.class
				.isAssignableFrom(PortletRegistryItemEntity.class));
	}

	@Test
	public void testToString() {
		String baseUrl = "bAsEuRl";
		item.setBaseUrl(baseUrl);
		String portletId = "PoRtLeTiD";
		item.setPortletId(portletId);
		String serviceName = "sErViCeNaMe";
		item.setServiceName(serviceName);
		assertEquals("baseURL: " + baseUrl + " portletId: " + portletId
				+ " serviceName: " + serviceName, item.toString());
	}
}

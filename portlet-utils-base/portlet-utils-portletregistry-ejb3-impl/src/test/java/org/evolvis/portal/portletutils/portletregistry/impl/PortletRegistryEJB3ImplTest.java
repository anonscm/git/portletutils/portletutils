/**
 * 
 */
package org.evolvis.portal.portletutils.portletregistry.impl;

import static org.mockito.Mockito.when;

import javax.portlet.PortletURL;

import org.evolvis.portal.portletutils.portletregistry.PortletURLProvider;
import org.evolvis.portal.portletutils.portletregistry.exception.PortletRegistryException;
import org.evolvis.portal.portletutils.portletregistry.exception.ServiceUnknownException;
import org.junit.Before;
import org.junit.Test;

import com.bm.testsuite.BaseSessionBeanFixture;

/**
 * @author pcorne
 * 
 */
public class PortletRegistryEJB3ImplTest extends
		BaseSessionBeanFixture<PortletRegistryEJB3Impl> {
	private static final Class<?>[] usedBeans = { PortletRegistryServiceEjb3Impl.class };

	public PortletRegistryEJB3ImplTest() {
		super(PortletRegistryEJB3Impl.class, usedBeans);
	}

	private PortletRegistryEJB3Impl registry;

	/**
	 * @throws java.lang.Exception
	 */
	@Override
	@Before
	public void setUp() throws Exception {
		super.setUp();
		registry = getBeanToTest();
		registry
				.setPortletRegistryService(new org.evolvis.portal.portletutils.portletregistry.impl.PortletRegistryServiceMapImpl());
		registry
				.registerPortletInstance(new PortletRegistryItemEntity(
						"http://local.host:9090/test", "newPortletId",
						"newServiceName","context"));
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#createActionUrl(java.lang.String)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testCreateUrls() throws PortletRegistryException {
		PortletURL resultUrlMock = org.mockito.Mockito.mock(PortletURL.class);
		when(resultUrlMock.toString()).thenReturn("peng");

		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		when(
				mockProvider.createActionUrl("http://local.host:9090/test",
						"newPortletId")).thenReturn(resultUrlMock);

		registry.setPortletURLProvider(mockProvider);
		assertEquals(resultUrlMock, registry.createActionUrl("newServiceName"));
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#createRenderUrl(java.lang.String)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testCreateRenderUrl() throws PortletRegistryException {
		PortletURL resultUrlMock = org.mockito.Mockito.mock(PortletURL.class);
		when(resultUrlMock.toString()).thenReturn("peng");

		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		when(
				mockProvider.createRenderUrl("http://local.host:9090/test",
						"newPortletId")).thenReturn(resultUrlMock);

		registry.setPortletURLProvider(mockProvider);
		assertEquals(resultUrlMock, registry.createRenderUrl("newServiceName"));
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#createResourceUrl(java.lang.String)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testCreateResourceUrl() throws PortletRegistryException {
		PortletURL resultUrlMock = org.mockito.Mockito.mock(PortletURL.class);
		when(resultUrlMock.toString()).thenReturn("peng");

		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		when(
				mockProvider.createResourceUrl("http://local.host:9090/test",
						"newPortletId")).thenReturn(resultUrlMock);

		registry.setPortletURLProvider(mockProvider);
		assertEquals(resultUrlMock, registry
				.createResourceUrl("newServiceName"));
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#registerPortletInstance(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testRegisterPortletInstance() throws PortletRegistryException {
		// check if another service is working too
		registry.registerPortletInstance(new PortletRegistryItemEntity(
				"http://localhost:9090/test", "newPortletId2",
				"newServiceName2","context"));
		PortletURL resultUrlMock = org.mockito.Mockito.mock(PortletURL.class);
		when(resultUrlMock.toString()).thenReturn("peng");

		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		when(
				mockProvider.createResourceUrl("http://localhost:9090/test",
						"newPortletId2")).thenReturn(resultUrlMock);

		registry.setPortletURLProvider(mockProvider);
		assertEquals(resultUrlMock, registry
				.createResourceUrl("newServiceName2"));
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#registerPortletInstance(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testMissingPortletURLProvider() throws PortletRegistryException {
		registry.setPortletURLProvider(null);
		try {
			registry.createResourceUrl("newServiceName");
			fail();
		} catch (PortletRegistryException e) {
			e.getClass();
		}
	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#registerPortletInstance(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testFetchUnknownService() throws PortletRegistryException {
		// check if another service is working too
		registry.registerPortletInstance(new PortletRegistryItemEntity(
				"http://localhost:9090/test", "newPortletId2",
				"newServiceName2","context"));
		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		registry.setPortletURLProvider(mockProvider);
		try {
			registry.createResourceUrl("newServiceName3");
			fail();
		} catch (ServiceUnknownException e) {

		}

	}

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#registerPortletInstance(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testGetRegistryItemForService() throws PortletRegistryException {
		// check if another service is working too
		registry.registerPortletInstance(new PortletRegistryItemEntity(
				"http://localhost:9090/test", "newPortletId2",
				"newServiceName2","context"));
		assertEquals("http://localhost:9090/test", registry
				.getRegistryItemForService("newServiceName2").getBaseUrl());
		assertEquals("newPortletId2", registry.getRegistryItemForService(
				"newServiceName2").getPortletId());
		assertEquals("newServiceName2", registry.getRegistryItemForService(
				"newServiceName2").getServiceName());
		assertNull(registry.getRegistryItemForService("newServiceName3"));
	}
	

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#registerPortletInstance(org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryItemEntity)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testGetRegistryItemForService_02() throws PortletRegistryException {
		// check if another service is working too
		registry.registerPortletInstance(new PortletRegistryItemEntity(
				"http://localhost:9090/test", "newPortletId2",
				"newServiceName2","context"));
		assertEquals("http://localhost:9090/test", registry
				.getRegistryItemForService("newServiceName2","context").getBaseUrl());
		assertEquals("newPortletId2", registry.getRegistryItemForService(
				"newServiceName2","context").getPortletId());
		assertEquals("newServiceName2", registry.getRegistryItemForService(
				"newServiceName2","context").getServiceName());
		assertNull(registry.getRegistryItemForService("newServiceName2","context2"));
	}
	

	/**
	 * Test method for
	 * {@link org.evolvis.portal.portletutils.portletregistry.sun.PortletRegistryEJB3Impl#unregisterPortletInstance(java.lang.String)}
	 * .
	 * 
	 * @throws PortletRegistryException
	 */
	@Test
	public void testUnregisterPortletInstance() throws PortletRegistryException {
		// check if another service is working too
		registry.registerPortletInstance(new PortletRegistryItemEntity(
				"http://localhost:9090/test", "newPortletId2",
				"newServiceName2","context"));
		PortletURLProvider mockProvider = org.mockito.Mockito
				.mock(PortletURLProvider.class);
		registry.setPortletURLProvider(mockProvider);
		try {
			registry.createResourceUrl("newServiceName2");
		} catch (ServiceUnknownException e) {
			fail();
		}
		registry.unregisterPortletInstance("newPortletId2");
		try {
			registry.createResourceUrl("newServiceName2");
			fail();
		} catch (ServiceUnknownException e) {
			e.getCause();
		}
	}

	@Test
	public void testCreatePortletRegistryItem() {
		PortletRegistryItemEntity entity = new PortletRegistryItemEntity(
				"baseurl", "portletid", "servicename","context");
		assertEquals(entity, registry.createRegistryItem(entity.getBaseUrl(),
				entity.getPortletId(), entity.getServiceName(),entity.getContext()));
	}
}

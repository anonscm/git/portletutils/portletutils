package org.evolvis.portal.portlet.utils.portletregistry;

import javax.portlet.PortletURL;

import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping(value = "VIEW")
public class CallerControler {

	@Autowired
	@Qualifier("portletRegistry")
	private PortletRegistry portletRegistry;
	
	@RenderMapping
	public String renderView(final Model model) {
		
		// get all
		model.addAttribute("portletRegistryItems", portletRegistry.getAllRegistryItems());
		
		try {
			// get CalleePortlet and generate a renderUrl
			PortletURL renderUrl = portletRegistry.createRenderUrl("CalleePortlet");
			model.addAttribute("calleePortletRenderUrl", renderUrl.toString());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return "caller";
	}
}

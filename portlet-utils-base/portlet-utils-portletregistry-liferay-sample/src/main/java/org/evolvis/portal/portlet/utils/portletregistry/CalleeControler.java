package org.evolvis.portal.portlet.utils.portletregistry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.evolvis.portal.portletutils.portletregistry.PortletRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping(value = "VIEW")
public class CalleeControler {

	@Autowired
	@Qualifier("portletRegistry")
	private PortletRegistry portletRegistry;

//	@ActionMapping(value = "register")
//	public void register(ActionRequest request, ActionResponse response) {
//		request.get
//	}
	
	@RenderMapping
	public String renderView() {
		return "callee";
	}
}
